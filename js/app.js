// 1) Функція setTimeout() запускається одноразово через зазначений проміжок часу, а функція setInterval()
// запускається періодично через зазначений інтервал часу.
// 2) Ні, тому що спочатку все одно виконується поточний код, а потім функція setTimeout().
// 3) Тому що поки існує setInterval(), сама ця функція і змінні, на які вона посилається продовжують займати
// пам'ять.

const img = document.querySelectorAll('.image-to-show');
const stopBtn = document.querySelector('.btn-stop');
const playBtn = document.querySelector('.btn-play');
let timerId;

stopBtn.disabled = false;
playBtn.disabled = false;

let active = 0;

function showImg(){
    img[active].classList.remove('active');
    if (active+1 >= img.length) {
        active = 0;
    }
    else {
        active++
    }
    img[active].classList.add('active')
}

function startInterval() {
    showImg();
    timerId = setInterval(showImg, 3000);
}

stopBtn.addEventListener('click',() => {
    clearInterval(timerId)
    stopBtn.disabled = true;
    playBtn.disabled = false;
})
playBtn.addEventListener('click',() => {
    clearInterval(timerId)
    timerId = setInterval(showImg, 3000);
    playBtn.disabled = true;
    stopBtn.disabled = false;

})
startInterval();

